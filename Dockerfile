FROM ruby:2.5

MAINTAINER msfloss_group@gmail.com

RUN apt-get upgrade && apt-get update && apt-get install -y ruby-dev ruby-all-dev rails nodejs

ADD build/run.sh issue-tracker-bot /issue-tracker-bot/

WORKDIR /issue-tracker-bot

RUN gem install nokogiri rails-dom-testing && bundle install

EXPOSE 3000

ENTRYPOINT ["/issue-tracker-bot/run.sh"]
