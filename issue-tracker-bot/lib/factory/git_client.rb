require 'net/http'
require 'json'
require_relative "gitlab_client"
require_relative "github_client"
require_relative "bugzilla_client"

module Factory

    class GitClient

        attr_reader :endpoint,
                    :owner,
                    :project,
                    :issues_domain,
                    :limit_remaining_access, #limit of access that you have in the endpoint
                                            #using private_token, github gives you 5000 call per hour
                    :private_token

        def initialize(type, params)

            puts params.inspect 

            case type
            when 'bugzilla'
                @client=BugzillaClient.new(
                    endpoint = params['endpoint'],
                    project = params['project'],
                    user = params['user'],
                    password = params['password'],
                    limit_issues = params['limit_issues']
                )
            when 'gitlab'
                @client=GitlabClient.new(
                    endpoint = params['endpoint'],
                    project = params['project'],
                    private_token = params['private_token']
                )
            when 'github'
                @client=GithubClient.new(
                    endpoint = params['endpoint'],
                    owner = params['owner'],
                    project = params['project'],
                    private_token = params['private_token'],
                )
            else
                raise 'Unsupported type of client'
            end
        end

        def load_issues
            return @client.list_issues
        end

        def load_comment_on_issues
            return @client.load_comment_on_issues
        end

        def get_issues_domain
            
            return @client.get_issues_domain
        end

    end
end