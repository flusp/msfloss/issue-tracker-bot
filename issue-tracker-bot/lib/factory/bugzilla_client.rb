#############################
### Reference: https://bugzilla.readthedocs.io/en/latest/api/index.html
#############################


require 'net/http'
require 'json'

require_relative  "../domain/base"
require_relative  "../domain/issue"
require_relative  "../domain/user"
require_relative  "../domain/comment"

class BugzillaClient

    attr_reader :endpoint,
                :owner,
                :project,
                :issues_domain,
                :user,
                :password
                :limit_issues

    def initialize(endpoint, project, user, password, limit_issues)
        @endpoint = endpoint
        @project = project
        @user = user
        @password = password
        @limit_issues = limit_issues

        @issues_domain = []
    end

    def get_issues_domain
        @issues_domain
    end

    def list_issues
        url_issues = "#{@endpoint}/rest/bug?login=#{@user}&password=#{@password}&product=#{@project}&limit=#{@limit_issues}"

        uri = URI(url_issues)
        response = Net::HTTP.get(uri)
        issues_json = JSON.parse(response)

        issues_json["bugs"].each do |issue|
             issue_domain = parse_issue_to_domain issue
             @issues_domain.push(issue_domain)
         end

         @issues_domain
    end

    def load_comment_on_issues

        @issues_domain.each do |issue|
            url_comments = "#{@endpoint}/rest/bug/#{issue.id}/comment?login=#{@user}&password=#{@password}&product=#{@project}"

            puts "collecting comments from issue number: #{issue.id}"

            uri = URI(url_comments)
            response = Net::HTTP.get(uri)
            comments_json = JSON.parse(response)

            comments = comments_json["bugs"][issue.id.to_s]["comments"]

            comments.each do |comment|
                comments_domain = parse_comment_to_domain comment
                issue.add_comment(comments_domain)
            end
        end
    end

    private
    def parse_issue_to_domain(issue)
        id_issue = issue["id"]
        created_at = issue["creation_time"]
        status = issue["status"]
        email = ""
        user = issue["creator_detail"]["name"]
        id_user = issue["creator_detail"]["id"]
        labels = issue["keyword"]
        title = ""
        user = Domain::User.new(id_user, user, email)
        return Domain::Issue.new(id_issue, user, created_at, status, title, labels)
    end


    private
    def parse_comment_to_domain(comment)
        id_comment = comment["id"]
        created_at = comment["creation_time"]
        email = ""
        user = comment["creator"]
        id_user = comment["count"] #seq of each comment inside an issue

        user = Domain::User.new(id_user, user, email)
        return Domain::Comment.new(id_comment, user, created_at)
    end

end
