require 'net/http'
require 'json'

require_relative  "../domain/base"
require_relative  "../domain/issue"
require_relative  "../domain/user"
require_relative  "../domain/comment"

class GithubClient

    attr_reader :endpoint,
                :owner,
                :project,
                :issues_domain,
                :private_token

    @@limit_remaining_access = 0

    #endpoint --> https://api.github.com
    #owner --> piotrmurach
    #project -->  github

    def initialize(endpoint, owner, project, private_token)
        @endpoint = endpoint
        @owner = owner
        @project = project
        @private_token = private_token

        @issues_domain = []
    end

    def get_issues_domain
        @issues_domain
    end

    def list_issues
        url_issues = "#{@endpoint}/repos/#{@owner}/#{project}/issues?state=all"

        uri = URI.parse(url_issues)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        request = Net::HTTP::Get.new(uri)
        request['Authorization'] = "Bearer #{private_token}"
        response = http.request(request)

        #calculating remaining rate access limit you have available
        puts "remaining rate access limit you have available: #{response["X-RateLimit-Remaining"]} of #{response["X-RateLimit-Limit"]}"

        @@limit_remaining_access = response["X-RateLimit-Remaining"];

        issues_json = JSON.parse(response.read_body)

        issues_json.each do |issue|
            issue_domain = parse_issue_to_domain issue
            @issues_domain.push(issue_domain)
        end

        @issues_domain
    end

    def load_comment_on_issues

        @issues_domain.each do |issue|
            url_comments = "#{@endpoint}/repos/#{@owner}/#{project}/issues/#{issue.id}/comments"

            puts "collecting comments from issue number: #{issue.id}"

            uri = URI.parse(url_comments)
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            request = Net::HTTP::Get.new(uri)
            request['Authorization'] = "Bearer #{private_token}"
            response = http.request(request)

            #calculating remaining rate access limit you have available
            puts "remaining rate access limit you have available: #{response["X-RateLimit-Remaining"]} of #{response["X-RateLimit-Limit"]}"

            @@limit_remaining_access = response["X-RateLimit-Remaining"];

            comments = JSON.parse(response.read_body)

            comments.each do |comment|
                comments_domain = parse_comment_to_domain comment
                issue.add_comment(comments_domain)
            end
        end
    end

    private
    def parse_issue_to_domain(issue)
        id_issue = issue["number"]
        created_at = issue["created_at"]
        status = issue["state"]
        email = ""
        title = ""
        labels = issue["labels"]
        user = issue["user"]["login"]
        id_user = issue["user"]["id"]

        user = Domain::User.new(id_user, user, email)
        return Domain::Issue.new(id_issue, user, created_at, status, title, labels)
    end


    private
    def parse_comment_to_domain(comment)
        id_comment = comment["id"]
        created_at = comment["created_at"]
        email = ""
        user = comment["user"]["login"]
        id_user = comment["user"]["id"]

        user = Domain::User.new(id_user, user, email)
        return Domain::Comment.new(id_comment, user, created_at)
    end
end
