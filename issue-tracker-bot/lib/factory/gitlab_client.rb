require 'net/http'
require 'json'

require_relative  "../domain/base"
require_relative  "../domain/issue"
require_relative  "../domain/user"
require_relative  "../domain/comment"

class GitlabClient

    attr_reader :endpoint,
                :project,
                :private_token,
                :issues_domain

    @@limit_remaining_access = 0

    #endpoint --> https://gitlab.com/api/v4
    #project -->  9628517
    def initialize(endpoint, project, private_token)
        @endpoint = endpoint
        @project = project
        @private_token = private_token

        @issues_domain = []
    end

    def get_issues_domain
        @issues_domain
    end

    def list_issues
        url_issues = "#{@endpoint}/projects/#{project}/issues?private_token=#{private_token}&per_page=100"
        uri = URI.parse(url_issues)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        request = Net::HTTP::Get.new(uri)
        response = http.request(request)
        issues_json = JSON.parse(response.read_body)

        #calculating remaining rate access limit you have available
        puts "remaining rate access limit you have available: #{response["ratelimit-remaining"]} of #{response["ratelimit-limit"]}"

        @@limit_remaining_access = response["ratelimit-remaining"]

        issues_json.each do |issue|
            issue_domain = parse_issue_to_domain issue
            @issues_domain.push(issue_domain)
        end

        @issues_domain
    end

    def load_comment_on_issues

        @issues_domain.each do |issue|
            url_comments = "#{@endpoint}/projects/#{project}/issues/#{issue.id}/notes?private_token=#{private_token}&per_page=100"

            puts url_comments
            puts "collecting comments from issue number: #{issue.id}"

            uri = URI.parse(url_comments)
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            request = Net::HTTP::Get.new(uri)
            response = http.request(request)

            comments = JSON.parse(response.read_body)

            #calculating remaining rate access limit you have available
            puts "remaining rate access limit you have available: #{response["ratelimit-remaining"]} of #{response["ratelimit-limit"]}"

            @@limit_remaining_access = response["ratelimit-remaining"]

            comments.each do |comment|
                comments_domain = parse_comment_to_domain comment
                issue.add_comment(comments_domain)
            end
        end
        @issues_domain
    end


    private
    def parse_issue_to_domain(issue)
        id_issue = issue["iid"]
        created_at = issue["created_at"]
        status = issue["state"]
        email = ""
        user = issue["author"]["username"]
        id_user = issue["author"]["id"]
        title = ""
        labels = issue["labels"]
        user = Domain::User.new(id_user, user, email)
        return Domain::Issue.new(id_issue, user, created_at, status, title, labels)
    end


    private
    def parse_comment_to_domain(comment)

        id_comment = comment["id"]
        created_at = comment["created_at"]
        email = ""
        user = comment["author"]["username"]
        id_user = comment["author"]["id"]

        user = Domain::User.new(id_user, user, email)
        return Domain::Comment.new(id_comment, user, created_at)
    end
end
