require 'json'
require_relative  "repository_base"

module Repository
    class IssuesRepositoryRepository < RepositoryBase

        #collection name
        attr_reader :collection, :client

        def initialize()
            super()
            @client = getClient
            @collection = "repositories".to_sym
        end

        def insertRepository(repository)
            result = insertOne(@collection, repository)
            return getIdLastInserted
        end

        def getIdLastInserted()
            result = @client[@collection].find({}, { 'projection' => { '_id' => true } }).sort("_id" => -1).limit(1).first
        end

        def deleteRepository(repositoryId)
            delete(@collection, {:_id => BSON::ObjectId(repositoryId)})
        end

        def updateRepository(repositoryId, repository)

            begin
                update(@collection, {:_id => BSON::ObjectId(repositoryId)}, repository)
            rescue StandardError => e
                puts e.message
                puts e.backtrace.inspect
            end
        end

        def listAllRepositories()
            listAll(@collection)
        end

        def getRepositorybyId(repositoryId)
            @client[@collection].find({:_id => BSON::ObjectId(repositoryId)})
        end

        def getIssues(repositoryId)
            # sugestão para a query, vejam o que acham

            # issue_repo = IssueRepository.new
            # repo = getRepositorybyId repositoryId

            # ####recupera a issues
            # issues = repo.first[:issues]

            # jsonlist = []
            # # ###convert cada issue para BSON
            # issues.each do |issue|
            #     jsonlist.push issue_repo.getIssuebyId(issue.to_i)
            # end
            # jsonlist

            match = { "$match" =>   {
                                        "_id": BSON::ObjectId.from_string(repositoryId.to_s)
                                    } 
                    }

            unwind = { "$unwind" => "$issues" }

            addFields = { "$addFields" =>  {
                    "issueId": { "$toObjectId": "$issues" }
                    }
            }

            lookup = { "$lookup" => {
                            "from": "issues",
                            "localField": "issueId",
                            "foreignField": "_id",
                            "as": "issue"
                        }
            }

            unwind2 = { "$unwind" => "$issue" }

            project = { "$project" => {
                                            "_id": false,
                                            "id": "$issue.id",
                                            "user": "$issue.user",
                                            "comments": "$issue.comments",
                                            "status": "$issue.status",
                                            "title": "$issue.title",
                                            "created_at": "$issue.created_at"
                                        }
                        }

            @client[@collection].aggregate([match, unwind, addFields, lookup, unwind2, project])
        end

        def getUsers(repositoryId)
            match = { "$match" =>   {
                                        "_id": BSON::ObjectId.from_string(repositoryId.to_s)
                                    } 
                    }

            unwind = { "$unwind" => "$issues" }

            addFields = { "$addFields" =>  {
                                "issueId": { "$toObjectId": "$issues" }
                                }
                        }

            lookup = { "$lookup" => {
                                        "from": "issues",
                                        "localField": "issueId",
                                        "foreignField": "_id",
                                        "as": "issue"
                                    }
                    }

            project = { "$project" => {
                                            "_id": false,
                                            "login": "$issue.user.login"
                                        }
                        }

            unwind2 = { "$unwind" => "$login" }

            @client[@collection].aggregate([match, unwind, addFields, lookup, project, unwind2])
        end

    end
end
