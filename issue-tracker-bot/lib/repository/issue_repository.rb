require 'json'
require_relative  "repository_base"

module Repository
    class IssueRepository < RepositoryBase

        #collection name
        attr_reader :collection, :client_issue


        def initialize()
            super()
            @client_issue = getClient
            @collection = "issues".to_sym
        end

        def insertIssues(issuesDominio)
            insertMany(@collection, issuesDominio)
        end

        def delete_and_insert_if_exist(issueDominio)
            @client_issue[@collection].find(:id => issueDominio.id).find_one_and_delete
            result = @client_issue[@collection].insert_one(parse(issueDominio))
            return getIdLastInserted
        end

        def deleteIssue(issueId)
            delete(@collection, issueId)
        end

        def updateIssue(issueId, issueDominio)
            update(@collection, issueId, issueDominio)
        end

        def listAllIssues()
            listAll(@collection)
        end

        def getIssuebyId(issueId)
            @client[@collection].find({:id => issueId}, {:projection => {"_id":0}})
        end

        def getIdLastInserted()
            @client[@collection].find({}, { 'projection' => { '_id' => true } }).sort("_id" => -1).limit(1).first
        end

        def getIssuesByComments()
            group = {"$group" => {
                            "_id": {"id": "$id", "title": "$title"},
                            "comments": {
                                            "$sum": {
                                                "$size": "$comments"
                                            }
                                        }
                            }
                        }

            sort = {"$sort" => {
                                    "comments": -1
                                }
                            }

            project = {"$project" => {
                                        "_id": false,
                                        "issueId": "$_id.id",
                                        "issueTitle": "$_id.title",
                                        "comments": "$comments"
                                    }
                        }

            @client[@collection].aggregate([group, sort, project])
        end

        def getIssuesClosedGroupedByUser()
            group = {"$group" => {
                                "_id": "$user",
                                        "issuesClosed": {
                                            "$sum": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": [ "$status", "closed" ] },
                                                            "then": 1
                                                        }
                                                    ],
                                                    "default": 0
                                                }
                                            }
                                        }
                            }
                        }

            sort = {"$sort" => {
                                "issuesClosed": -1
                                }
                            }

            project = {"$project" => {
                                    "_id": false,
                                    "id": "$_id.id",
                                    "login": "$_id.login",
                                    "email": "$_id.email",
                                    "issuesClosed": "$issuesClosed"
                                    }
                        }

            @client[@collection].aggregate([group, sort, project])
        end

        def getIssuesCreatedGroupedByUser()
            group = {"$group" => {
                                "_id": "$user",
                                        "issuesCreated": {
                                            "$sum": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": [ "$status", "created" ] },
                                                            "then": 1
                                                        }
                                                    ],
                                                    "default": 0
                                                }
                                            }
                                        }
                            }
                        }

            sort = {"$sort" => {
                                "issuesCreated": -1
                                }
                            }

            project = {"$project" => {
                                    "_id": false,
                                    "id": "$_id.id",
                                    "login": "$_id.login",
                                    "email": "$_id.email",
                                    "issuesCreated": "$issuesCreated"
                                    }
                        }

            @client[@collection].aggregate([group, sort, project])
        end

        def getIssuesGroupedByUser()
            group = {"$group" => {
                                    "_id": "$user",
                                            "issuesClosed": {
                                                "$sum": {
                                                    "$switch": {
                                                        "branches": [
                                                            {
                                                                "case": { "$eq": [ "$status", "closed" ] },
                                                                "then": 1
                                                            }
                                                        ],
                                                        "default": 0
                                                    }
                                                }
                                            },
                                            "issuesCreated": {
                                                "$sum": {
                                                    "$switch": {
                                                        "branches": [
                                                            {
                                                                "case": { "$eq": [ "$status", "created" ] },
                                                                "then": 1
                                                            }
                                                        ],
                                                        "default": 0
                                                    }
                                                }
                                            },
                                            "issuesCommented": {
                                                "$sum": {
                                                    "$switch": {
                                                        "branches": [
                                                            {
                                                                "case": { "$gt": [ { "$size": "$comments"}, 0] },
                                                                "then": 1
                                                            }
                                                        ],
                                                        "default": 0
                                                    }
                                                }
                                            }
                                }
                        }

            project = {"$project" => {
                                        "_id": false,
                                        "id": "$_id.id",
                                        "login": "$_id.login",
                                        "email": "$_id.email",
                                        "issuesClosed": "$issuesClosed",
                                        "issuesCreated": "$issuesCreated",
                                        "issuesCommented": "$issuesCommented"
                                        }
                        }

            @client[@collection].aggregate([group, project])
        end

        def getIssuesGroupedByUserOnRepo(repositoryId, userId)

            ###instancia o repositorio
            repository = IssuesRepositoryRepository.new
            ###pega o repo por id
            repo = repository.getRepositorybyId repositoryId

            ####recupera a issues
            listIssues = repo.first[:issues]

            ##criar um filtro
            filter = []

            ###convert cada issue para BSON
            listIssues.each do |issue|
                filter.push BSON::ObjectId(issue)
            end

            userId

            @client[@collection].find(
                {"$and" => [{:_id => {"$in" => filter} },
                            {"user.id" => userId.to_i} ]}
            )
        end
    end
end
