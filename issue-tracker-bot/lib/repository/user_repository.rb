require 'json'
require_relative  "repository_base"

module Repository
    class UserRepository < RepositoryBase

        #collection name
        attr_reader :collection

        def initialize()
            super()
            @collection = "users".to_sym
        end

        def insertUser(userDominio)
            insertOne(@collection, userDominio)
        end

        def deleteUser(userId)

        end

        def updateUser(userId, userDominio)

        end

        def listAllUsers()
            listAll(@collection)
        end

        def getUserbyId(userId)

        end

        def getUsersActivities(countAuthors)
            unwind = {"$unwind": "$comments" }

            group = {"$group" => {
                                    "_id": "$comments.user.login",
                                    "countComments": {
                                        "$sum": 1
                                    }
                                }
                    }

            lookup = {"$lookup" =>  {
                                        "from": 'issues',
                                        "localField": '_id',
                                        "foreignField": 'user.login',
                                        "as": 'issues'
                                    }
                        }

            project = {"$project" =>  {
                                        "_id": 0,
                                        "login": "$_id",
                                        "countComments": 1,
                                        "countIssues": { "$size": "$issues" }
                                    }
                        }

            sort = {"$sort" =>  {
                                    "countComments": -1
                                }
                    }

            if countAuthors.nil?
                @client["issues"].aggregate([unwind, group, lookup, project, sort])
            else
                limit = {"$limit": countAuthors.to_i }
                @client["issues"].aggregate([unwind, group, lookup, project, sort, limit])
            end
        end

        def getUserDetail(login)

            match = {"$match" =>  {
                                        "user.login": login
                                }
                    }

            project = {"$project" =>  {
                                        "_id": 0
                                    }
                        }


            @client["issues"].aggregate([match, project])            
            
        end

    end
end