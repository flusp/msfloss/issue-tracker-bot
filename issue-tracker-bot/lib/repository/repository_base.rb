#http://zetcode.com/db/mongodbruby/
#https://medium.com/@yl.hoony/rails-api-only-api-with-mongodb-e53fee6ec0d6
#https://docs.mongodb.com/ruby-driver/master/tutorials/ruby-driver-crud-operations/#reading

require 'mongo'
require 'json'

class RepositoryBase

    attr_reader :client

    def initialize()

        Mongo::Logger.logger.level = ::Logger::FATAL

        @client = Mongo::Client.new([ ENV["MONGO_HOST"] ], user: ENV["MONGO_USERNAME"], password: ENV["MONGO_PASSWORD"])
        @client = @client.use("issuestracker")

    end

    def getClient
        @client
    end

    def listAll(collection)
        @client[collection].find()
    end

    def closeConnection()
        @client.close
    end

    def filterBy(collection, column, filter)
        @client[collection].find(column => filter)
    end

    def insertOne(collection, document)
        @client[collection].insert_one parse(document)
    end

    def insertMany(collection, documents)
        result = @client[collection].insert_many(parse(documents))
        result.inserted_count
    end

    #client[:cars].delete_one({:name => "Skoda"})
    def delete(collection, filter)
        @client[collection].delete_one(filter)
    end

    #client[:cars].update_one({:name => "Audi"}, '$set' => {:price => 52000})
    def update(collection, filter, data)
        @client[collection].update_one(filter, '$set' => parse(data))
    end

    private
    def parse(data)
        JSON.parse(data.to_json)
    end

end