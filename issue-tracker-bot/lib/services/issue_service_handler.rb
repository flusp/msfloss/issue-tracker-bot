require_relative  "../domain/base"
require_relative  "../domain/issues_repository"
require_relative  "../domain/issue"
require_relative  "../domain/user"
require_relative  "../domain/comment"

module Services
    class IssueServiceHandler
        def initialize()  
            @repositoryRepository = Repository::IssuesRepositoryRepository.new
        end

        public
        def handleClients()

            #handle all clients 
            begin 
                repositories = @repositoryRepository.listAllRepositories

                repositories.each do |rep|

                    case rep[:client]
                    when "github"
                        handlerGithub rep
                    when "gitlab"
                        handlerGitlab rep
                    when "bugzilla"
                        handlerBugzilla rep
                    else
                      raise "There's not #{rep[:client]} client available. "
                    end
    
                end

            rescue StandardError => e  
                puts e.message  
                puts e.backtrace.inspect 

                return {status: "Error!" } 
            end

            return {status: "Service started!" }

        end

        private
        def handlerGithub(rep)

            ##params = { "endpoint" => "https://api.github.com", "project" => "github", "owner" => "piotrmurach", "token" => "d5f90fbc8e26d45638ead3167b35def2fc180e32" } 

            params = { 
                       "endpoint" => rep[:url],
                       "project" => rep[:project], 
                       "owner" => rep[:owner], 
                       "private_token" => rep[:private_token]
                    } 
                    
            client = Factory::GitClient.new('github', params)
            client.load_issues
            client.load_comment_on_issues.inspect
            issues = client.get_issues_domain

            handleIssues issues
            handleRepository rep, issues
            
        end

        private
        def handlerGitlab(rep)

            #params = { "endpoint" => "https://gitlab.com/api/v4", "project" => "9628517", "private_token" => "wNygMmMkFYiTsLnBJbkx" }

            params = { 
                       "endpoint" => rep[:url],
                       "project" => rep[:project], 
                       "private_token" => rep[:private_token]
                    } 

            client = Factory::GitClient.new('gitlab', params)
            client.load_issues
            client.load_comment_on_issues.inspect
            issues = client.get_issues_domain

            handleIssues issues
            handleRepository rep, issues
            
        end

        private
        def handlerBugzilla(rep)

            #params = { "endpoint" => "https://bugzilla.kernel.org", "project" => "ACPI", "user" => "agnei.silva@usp.br", "password" => "Agnei.Silva@2019", "limit_issues" => 50 }

            params = {
                      "endpoint" => rep[:url], 
                      "project" => rep[:project], 
                      "user" => rep[:user], 
                      "password" => rep[:password], 
                      "limit_issues" => rep[:limit_issues]
                    }

            client = Factory::GitClient.new('bugzilla', params)
            client.load_issues
            client.load_comment_on_issues.inspect
            issues = client.get_issues_domain

            handleIssues issues
            handleRepository rep, issues
            
        end

        private
        def handleIssues(issues)
            issueRepository= Repository::IssueRepository.new
             
            issues.each do |issue|
                result = issueRepository.delete_and_insert_if_exist issue
            
                if result.nil? 
                    raise "An error has occured during insert issue into database"
                end

                issue.id = result #expected to return id inserted
            end

            issueRepository.closeConnection   
            
        end

        private
        def handleRepository(rep, issues)
            @repositoryRepository = Repository::IssuesRepositoryRepository.new

            repoDomain = Domain::IssuesRepository.new rep[:client], rep[:url], rep[:project], rep[:owner], rep[:user], rep[:password], rep[:private_token], rep[:limit_issues]

            issues.each do |issue|
               repoDomain.add_issue issue.id[:_id].to_s
            end

            puts repoDomain.inspect

            @repositoryRepository.updateRepository rep[:_id], repoDomain

            @repositoryRepository.closeConnection            
        end
    end
end