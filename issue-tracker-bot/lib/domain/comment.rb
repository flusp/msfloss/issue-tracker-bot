module Domain
    class Comment < Base

        attr_accessor :user, :created_at

        def initialize(id, user, created_at)
            super(id)
            @user = user         
            @created_at = created_at
        end
    end
end