module Domain
    class Base

        attr_accessor :id

        def initialize(id)
            @id = id
        end
    end
end