module Domain
    class Issue < Base
        attr_accessor :user, :created_at, :status, :title

        def initialize(id, user, created_at, status, title, labels)
            super(id)
            @user = user
            @created_at = created_at
            @status = status
            @title = title
            @labels = labels
            @comments = []
        end

        def add_comment(comment)
            @comments.push(comment)
        end

        def comments
            @comments
        end
    end
end
