module Domain
    class User < Base

        attr_accessor :login, :email

        def initialize(id, login, email)
            super(id)
            @login = login
            @email = email         
        end
    end
end