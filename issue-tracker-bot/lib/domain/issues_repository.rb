module Domain
    class IssuesRepository < Base
    
        attr_accessor :client, :url, :project, :owner, :user, :password, :private_token, :limit_issues, :dt_create

        def initialize(client, url, project, owner, user, password, private_token, limit_issues)
            super(id)
 
            unless (['github', 'gitlab', 'bugzilla'].include? client)
                raise "Client #{client} not supported."
            end
 
             if(url.to_s.empty?)
                 raise "Url shouldn`t be empty."
             end
 
            if(project.to_s.empty?)
                raise "Project shouldn`t be empty."
            end
 
            if((user.to_s.empty? && password.to_s.empty?) && private_token.to_s.empty?)
                 raise "use at least one way auth mode (user and password or token)"
             end
         
            @client= client 
            @url = url 
            @project= project 
            @owner = owner
            @user = user
            @password = password 
            @private_token = private_token 
            @limit_issues = limit_issues
            @dt_create = Time.new
 
            @issues = []      
        end

        def add_issue(issue)
            @issues.push(issue)
        end

        def issues
            @issues
        end

    end
end