Rails.application.routes.draw do

  ###issues
  get '/issues', to: 'api/v1/issues#index'
  get '/issues/stats', to: 'api/v1/issues#stats'
  get '/issues/stats/users', to: 'api/v1/issues#users_stats'
  get '/issues/ranking/users-create', to: 'api/v1/issues#ranking_users_creators'
  get '/issues/ranking/users-close', to: 'api/v1/issues#ranking_users_close'
  get '/issues/ranking/issue-activity', to: 'api/v1/issues#ranking_issues_activity'
  get '/issues/:issueId', to: 'api/v1/issues#issues_byid'

  ###service
  get '/start-service', to: 'api/v1/service#start_service'

  ###repository
  get '/repositories', to: 'api/v1/repository#getAll'
  get '/repository/:repositoryId', to: 'api/v1/repository#getById'
  post '/repository', to: 'api/v1/repository#create'
  put '/repository/:repositoryId', to: 'api/v1/repository#update'
  delete '/repository/:repositoryId', to: 'api/v1/repository#delete'

  get '/repository/:repositoryId/authors/:authorId', to: 'api/v1/issues#users_stats_on_repo'
  get '/repository/:repositoryId/issues', to: 'api/v1/repository#getIssues'
  get '/repository/:repositoryId/authors', to: 'api/v1/repository#getAuthors'

  ###authors 
  get '/authors', to: 'api/v1/authors#list_authors'
  get '/authors/:login', to: 'api/v1/authors#detail_author', constraints: { login: /[^\/]+/} , as: 'detail_author'

  ###documentation
  get '/issue-tracker/doc', to: redirect('issue-tracker/swagger/dist/index.html')
end
