require 'rufus-scheduler'

scheduler = Rufus::Scheduler::singleton

##start service every 6 hours
##if needed force start service use route (/start-service) by HTTP GET 
scheduler.every '6h' do
    service = Services::IssueServiceHandler.new
    render :json => service.handleClients
end
