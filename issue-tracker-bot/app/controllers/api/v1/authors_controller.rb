class Api::V1::AuthorsController < ApplicationController

    def list_authors
        countAuthors = params[:count]
        repository = Repository::UserRepository.new
        render :json => repository.getUsersActivities(countAuthors)
    end

    def detail_author
        login = params[:login]
        repository = Repository::UserRepository.new
        render :json => repository.getUserDetail(login)
    end
end