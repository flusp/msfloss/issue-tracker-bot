class Api::V1::RepositoryController < ApplicationController
    skip_before_action :verify_authenticity_token

    def initialize()
      @repository = Repository::IssuesRepositoryRepository.new
    end

    def getIssues
        render :json => @repository.getIssues(params[:repositoryId])
    end
    # GET /repositories
    def getAll
      render :json => @repository.listAllRepositories
    end

    # GET /repository/{repositoryId}
    def getById
      render :json => @repository.getRepositorybyId(params[:repositoryId])
    end

    def getAuthors
        render :json => @repository.getUsers(params[:repositoryId])
    end
    # POST /repository
    def create
      result = @repository.insertRepository repository

       unless result.nil?
         render json: result
       else
         render json: { errors: "An error has occurred during register a repository" },
                status: :error, code: 500
       end
      #render :json => {msg: "not implemented"}
    end

    # PUT /repository/{repositoryId}
    def update
      result = @repository.updateRepository params[:repositoryId], repository

      unless result.nil?
        render json: result
      else
        render json: { errors: "An error has occurred during update a repository" },
               status: :error, code: 500
      end
      #render :json => {msg: "not implemented"}
    end

    # DELETE /repository/{repositoryId}
    def delete
      result = @repository.deleteRepository params[:repositoryId]

      unless result.nil?
        render json: result
      else
        render json: { errors: "An error has occurred during delete a repository" },
               status: :error, code: 500
      end
      #render :json => {msg: "not implemented"}
    end


    private
    def repository
      Domain::IssuesRepository.new params[:client], params[:url], params[:project], params[:owner], params[:user], params[:password], params[:private_token], params[:limit_issues]
    end
end
