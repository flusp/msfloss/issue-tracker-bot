#require_dependency "GitClient"

class Api::V1::IssuesController < ApplicationController

    def index
        repository = Repository::IssueRepository.new
        all_issues = repository.listAllIssues()
        if params.has_key?(:contains)
            filter = params[:contains]
            filtered_issues = Array.new
            all_issues.each do |i|
                i.each_key do |k|
                    puts k, i[k]
                    if (k != "_id" and i[k] == filter)
                        filtered_issues << i
                    end
                end
            end
            render :json => filtered_issues
            return
        end
        if params.has_key?(:label)
            filter = params[:label]
            filtered_issues = Array.new
            all_issues.each do |i|
                if i.has_key?("labels") and i["labels"].include?(filter)
                    filtered_issues << i
                end
            end
            render :json => filtered_issues
            return
        end
        repository = Repository::IssueRepository.new
        render :json => all_issues
    end

    def stats
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuesGroupedByUser
    end

    def users_stats
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuesGroupedByUser
    end

    def users_stats_on_repo
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuesGroupedByUserOnRepo(params[:repositoryId], params[:authorId])
    end
    def ranking_users_creators
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuesCreatedGroupedByUser
    end

    def ranking_users_close
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuesClosedGroupedByUser
    end

    def ranking_issues_activity
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuesByComments
    end

    def issues_byid
        issueId = params[:issueId]
        repository = Repository::IssueRepository.new
        render :json => repository.getIssuebyId(issueId.to_i)
    end
end
