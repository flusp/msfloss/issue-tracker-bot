class Api::V1::ServiceController < ApplicationController
    skip_before_action :verify_authenticity_token

    def initialize()
      @repository = Repository::IssuesRepositoryRepository.new
    end

    def start_service()

        service = Services::IssueServiceHandler.new
        service.handleClients
        
        render :json => { msg: "Service Finished" }

    end

end
