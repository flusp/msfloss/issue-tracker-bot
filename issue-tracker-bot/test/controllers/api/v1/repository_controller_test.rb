require 'test_helper'

class Api::V1::RepositoryControllerTest < ActionController::TestCase
    test "should get issues for this repository id" do
        get :getIssues, {repositoryId:"5d0bf65988cbe81aa259b1d6"}

        assert_response :success
    end

    test "should list all repositories" do
        get :getAll

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal("github", json_response[0]["client"])
        assert_equal("github", json_response[0]["project"])
        assert_equal("piotrmurach", json_response[0]["owner"])
        assert_equal("", json_response[0]["user"])
    end

    test "should repositories for this repository id" do
        get :getById, {repositoryId:"5d0bf65988cbe81aa259b1d6"}

        assert_response :success
    end

    test "should get authors for this repository id" do
        get :getAuthors, {repositoryId:"5d0bf65988cbe81aa259b1d6"}

        assert_response :success
    end

    test "should be able to create new empty repository" do
        get :create, {
            client: "bugzilla",
            url: "https://bugzilla.kernel.org",
            project: "ACPI",
            owner: "foo",
            user: "foo",
            password: "123",
            private_token: "E5rf17G36af4hH5",
            limit_issues: 10
        }

        assert_response :success
    end

    test "should be able to one repository" do
        get :getAll

        assert_response :success

        json_response = JSON.parse(response.body)
        repoId = json_response[-1]["_id"]["$oid"]

        get :update, {
            repositoryId:repoId,
            client: "bugzilla",
            url: "https://bugzilla.kernel.org",
            project: "ACPI",
            owner: "foo",
            user: "foo",
            password: "123",
            private_token: "E5rf17G36af4hH5",
            limit_issues: 10
        }

        assert_response :success
    end

    test "should be able to delete this repository" do
        get :getAll

        assert_response :success

        json_response = JSON.parse(response.body)
        repoId = json_response[-1]["_id"]["$oid"]

        get :delete, {repositoryId:repoId}

        assert_response :success
    end

end
