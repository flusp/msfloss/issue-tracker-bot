require 'test_helper'

class Api::V1::AuthorsControllerTest < ActionController::TestCase

    test "should list authors" do
        get :list_authors

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_lte(447, json_response[0]["countComments"])
        assert_equal("lenb@kernel.org", json_response[0]["login"])
        assert_lte(16, json_response[0]["countIssues"])
    end

    test "should detail author" do
        get :detail_author, {login:"lenb@kernel.org"}

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal(711, json_response[0]["user"]["id"])
        assert_equal("lenb@kernel.org", json_response[0]["user"]["login"])
        assert_equal("", json_response[0]["user"]["email"])
        assert_equal("CLOSED", json_response[0]["status"])
    end

end
