require 'test_helper'

class Api::V1::IssuesControllerTest < ActionController::TestCase

    test "should get index of existing client" do
        mock = Minitest::Mock.new
        mock.expect(:list_issues, true)
        mock.expect(:load_comment_on_issues, true)

        params = {
           "client" => "example_client"
        }

        Factory::GitClient.stub :new, mock do
            client = Factory::GitClient.new
            assert client.list_issues
            assert client.load_comment_on_issues
        end

        assert_mock mock
    end

    test "should get stats" do
        get :stats

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal("", json_response[0]["email"])
        assert_lte(0, json_response[0]["issuesClosed"])
        assert_lte(0, json_response[0]["issuesCreated"])
        assert_lte(0, json_response[0]["issuesCommented"])
    end

    test "should get user_stats" do
        get :users_stats

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal("", json_response[0]["email"])
        assert_lte(0, json_response[0]["issuesClosed"])
        assert_lte(0, json_response[0]["issuesCreated"])
        assert_lte(0, json_response[0]["issuesCommented"])
    end

    test "should get ranking_users_creators" do
        get :ranking_users_creators

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal("", json_response[0]["email"])
        assert_lte(0, json_response[0]["issuesCreated"])
    end

    test "should get ranking_users_close" do
        get :ranking_users_close

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal("", json_response[0]["email"])
        assert_lte(26, json_response[0]["issuesClosed"])
    end

    test "should get ranking_issues_activity" do
        get :ranking_issues_activity

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_lte(10, json_response[0]["issueId"])
        assert_equal("", json_response[0]["issueTitle"])
        assert_lte(100, json_response[0]["comments"])
    end

    test "should get issues by id" do
        get :issues_byid , {"issueId":10}

        assert_response :success

        json_response = JSON.parse(response.body)
        assert_equal(10, json_response[0]["id"])
        assert_equal("", json_response[0]["title"])
        assert_equal("", json_response[0]["user"]["email"])
    end

end
