require_relative '../lib_test_helper'

class UserRepositoryTest < ActiveSupport::TestCase

    test "should insert user" do
        userDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:insertUser, true, [userDominio])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.insertUser(userDominio)
        end

        assert_mock mock
    end

    test "should delete user" do
        userId = 1
        mock = Minitest::Mock.new
        mock.expect(:deleteUser, true, [userId])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.deleteUser(userId)
        end

        assert_mock mock
    end

    test "should update user" do
        userId = 1
        userDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:updateUser, true, [userId, userDominio])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.updateUser(userId, userDominio)
        end

        assert_mock mock
    end

    test "should list all users" do
        mock = Minitest::Mock.new
        mock.expect(:listAllUsers, true)

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.listAllUsers
        end

        assert_mock mock
    end

    test "should get user by id" do
        userId = 1
        mock = Minitest::Mock.new
        mock.expect(:getUserbyId, true, [userId])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.getUserbyId(userId)
        end

        assert_mock mock
    end
end
