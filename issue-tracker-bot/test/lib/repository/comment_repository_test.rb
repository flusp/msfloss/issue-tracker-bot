require_relative '../lib_test_helper'

class CommentRepositoryTest < ActiveSupport::TestCase

    test "should insert comment" do
        commentDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:insertComment, true, [commentDominio])

        Repository::CommentRepository.stub :new, mock do
            repo = Repository::CommentRepository.new
            assert repo.insertComment(commentDominio)
        end

        assert_mock mock
    end

    test "should delete comment" do
        commentId = 1
        mock = Minitest::Mock.new
        mock.expect(:deleteComment, true, [commentId])

        Repository::CommentRepository.stub :new, mock do
            repo = Repository::CommentRepository.new
            assert repo.deleteComment(commentId)
        end

        assert_mock mock
    end

    test "should update comment" do
        commentId = 1
        commentDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:updateComment, true, [commentId, commentDominio])

        Repository::CommentRepository.stub :new, mock do
            repo = Repository::CommentRepository.new
            assert repo.updateComment(commentId, commentDominio)
        end

        assert_mock mock
    end

    test "should list all comments" do
        mock = Minitest::Mock.new
        mock.expect(:listAllComments, true)

        Repository::CommentRepository.stub :new, mock do
            repo = Repository::CommentRepository.new
            assert repo.listAllComments
        end

        assert_mock mock
    end

    test "should get comment by id" do
        commentId = 1
        mock = Minitest::Mock.new
        mock.expect(:getCommentbyId, true, [commentId])

        Repository::CommentRepository.stub :new, mock do
            repo = Repository::CommentRepository.new
            assert repo.getCommentbyId(commentId)
        end

        assert_mock mock
    end
end
