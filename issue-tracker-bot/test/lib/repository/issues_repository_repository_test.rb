require_relative '../lib_test_helper'

class IssuesRepositoryRepositoryTest < ActiveSupport::TestCase

    test "should insert repository" do
        repositoryDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:insertRepository, true, [repositoryDominio])

        Repository::IssuesRepositoryRepository.stub :new, mock do
            repo = Repository::IssuesRepositoryRepository.new
            assert repo.insertRepository(repositoryDominio)
        end

        assert_mock mock
    end


    test "should delete repository" do
        repositoryId = 1
        mock = Minitest::Mock.new
        mock.expect(:deleteRepository, true, [repositoryId])

        Repository::IssuesRepositoryRepository.stub :new, mock do
            repo = Repository::IssuesRepositoryRepository.new
            assert repo.deleteRepository(repositoryId)
        end

        assert_mock mock
    end

    test "should update repository" do
        repositoryId = 1
        repositoryDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:updateRepository, true, [repositoryId, repositoryDominio])

        Repository::IssuesRepositoryRepository.stub :new, mock do
            repo = Repository::IssuesRepositoryRepository.new
            assert repo.updateRepository(repositoryId, repositoryDominio)
        end

        assert_mock mock
    end

    test "should list all repositorys" do
        mock = Minitest::Mock.new
        mock.expect(:listAllRepositorys, true)

        Repository::IssuesRepositoryRepository.stub :new, mock do
            repo = Repository::IssuesRepositoryRepository.new
            assert repo.listAllRepositorys
        end

        assert_mock mock
    end

    test "should get repository by id" do
        repositoryId = 1
        mock = Minitest::Mock.new
        mock.expect(:getRepositorybyId, true, [repositoryId])

        Repository::IssuesRepositoryRepository.stub :new, mock do
            repo = Repository::IssuesRepositoryRepository.new
            assert repo.getRepositorybyId(repositoryId)
        end

        assert_mock mock
    end
end
