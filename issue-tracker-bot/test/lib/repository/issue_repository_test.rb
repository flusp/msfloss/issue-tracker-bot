require_relative '../lib_test_helper'

class IssueRepositoryTest < ActiveSupport::TestCase

    test "should insert issue" do
        issueDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:insertIssue, true, [issueDominio])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.insertIssue(issueDominio)
        end

        assert_mock mock
    end


    test "should delete issue" do
        issueId = 1
        mock = Minitest::Mock.new
        mock.expect(:deleteIssue, true, [issueId])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.deleteIssue(issueId)
        end

        assert_mock mock
    end

    test "should update issue" do
        issueId = 1
        issueDominio = "example"
        mock = Minitest::Mock.new
        mock.expect(:updateIssue, true, [issueId, issueDominio])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.updateIssue(issueId, issueDominio)
        end

        assert_mock mock
    end

    test "should list all issues" do
        mock = Minitest::Mock.new
        mock.expect(:listAllIssues, true)

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.listAllIssues
        end

        assert_mock mock
    end

    test "should get issue by id" do
        issueId = 1
        mock = Minitest::Mock.new
        mock.expect(:getIssuebyId, true, [issueId])

        Repository::IssueRepository.stub :new, mock do
            repo = Repository::IssueRepository.new
            assert repo.getIssuebyId(issueId)
        end

        assert_mock mock
    end
end
