require_relative '../lib_test_helper'

class IssueServiceHandlerTest < ActiveSupport::TestCase

    test "should initialize" do
        assert true
    end


    test "should handle Clients" do
        mock = Minitest::Mock.new
        mock.expect(:handlerGithub, true)
        mock.expect(:handlerGitlab, true)
        mock.expect(:handlerBugzilla, true)

        Services::IssueServiceHandler.stub :new, mock do
            service = Services::IssueServiceHandler.new
            assert service.send(:handlerGithub)
            assert service.send(:handlerGitlab)
            assert service.send(:handlerBugzilla)
        end

        assert_mock mock
    end
end
