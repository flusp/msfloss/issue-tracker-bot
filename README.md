# Issue Tracker Bot

The Issue Tracker Bot is a component of MsFLOSS system. It is responsible for
fetching information from different issue tracker systems (currently GitHub,
GitLab and Bugzilla) and provide an [API](http://msfloss.interscity.org/issue-tracker/doc) with processed data
related to these issues.

## Running

To run this component you will need to have Docker and docker-composed
installed on your system.

The following command will build a Docker image with the required files and
libraries.

> chmod +x build/run.sh

> docker-compose build

After that you will need to start the service by running:

> docker-compose up

## Info

This component was created as project of the course MAC0413/5714 (Advanced
Topics of Object Oriented Programming from IME-USP) in the first semester of
2019.

The team members were :
* Agnei Silva
* João Victorino
* Fernando Lemes
* Pedro Sola
* Jonas Arilho

API documentation by:
* Lucas Correa
